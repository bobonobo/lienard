use std::{thread, time};
use minifb::{Key, Window, WindowOptions};
//use num_traits::{Zero,One};

pub fn display(buffer: &Vec<Vec<u32>>, width: usize, height: usize){
    let mut window = Window::new(
        "Test - ESC to exit",
        width,
        height,
        WindowOptions::default(),
    ).unwrap_or_else(|e| { panic!("{}", e);});

    // Limit to max ~60 fps update rate
    window.limit_update_rate(Some(time::Duration::from_micros(16_600)));

    while window.is_open() && !window.is_key_down(Key::Escape) 
    {   
        for j in 0..buffer.len()
        {  
            window.update_with_buffer(&(buffer[j]), width, height).unwrap();
            thread::sleep(time::Duration::from_micros(16_600));
        }
    }

}
pub fn radius(width: usize, height: usize)->(Vec<Vec<f64>>,Vec<Vec<f64>>,Vec<Vec<f64>>,Vec<Vec<f64>>,Vec<Vec<f64>>)
{
    let mut radius_array= vec![vec![0.0;width]; height];
    let mut inverse_radius_array= vec![vec![0.0;width]; height];
    let mut inverse_radius_square_array= vec![vec![0.0;width]; height];
    let mut n_x= vec![vec![0.0;width]; height];
    let mut n_y= vec![vec![0.0;width]; height];

    let mut x_2;
    let mut y_2;
    let mut r_2;



    
    for i in 0..width
    {
        x_2 = (i*i) as f64;
        
        for j in 0..height
        {
            y_2 = (j*j) as f64;
            r_2 = x_2+y_2;

            radius_array[j][i]=r_2.sqrt();
            if r_2 !=0.0 
            {
                inverse_radius_array[j][i]=radius_array[j][i].powf(-1.0);
                inverse_radius_square_array[j][i]=inverse_radius_array[j][i].powf(2.0);
                
                n_x[j][i]= (i as f64)*inverse_radius_array[j][i];
                n_y[j][i]= (j as f64)*inverse_radius_array[j][i];
            }else
            {
                inverse_radius_array[j][i]=0.0;
                inverse_radius_square_array[j][i]=0.0;
                n_x[j][i]=0.0;
                n_y[j][i]=0.0;
            }
        }
    } 
    return (radius_array,inverse_radius_array,inverse_radius_square_array,n_x,n_y);
}


pub fn motion(width: usize, height: usize, duration:usize)->(Vec<i32>,Vec<i32>,Vec<f64>,Vec<f64>,Vec<f64>,Vec<f64>)
{
    let mut x: Vec<i32> = vec![0;duration];
    let mut y: Vec<i32> = vec![0;duration];
    let mut v_x: Vec<f64> = vec![0.0;duration];
    let mut v_y: Vec<f64> = vec![0.0;duration];
    let mut a_x: Vec<f64> = vec![0.0;duration];
    let mut a_y: Vec<f64> = vec![0.0;duration];
    let mut r_cos;
    let mut r_sin;
    let omega= 1.0/40.0;
    let circle_radius= (height as f64)/8.0;
    for t in 0..duration
    {   
        r_cos= (circle_radius*((t as f64)*omega).cos()).round() as i32;
        r_sin= (circle_radius*((t as f64)*omega).sin()).round() as i32;
        x[t]=(width  as i32)/2+ r_cos;
        y[t]=(height as i32)/2+ r_sin;
        v_x[t]=-(r_sin as f64)*omega;
        v_y[t]=(r_cos as f64)*omega;
        a_x[t]=-v_y[t]*omega;
        a_y[t]=v_x[t]*omega;
        //println!("v_x:{}",v_x[t]);
        //println!("v_y:{}",v_y[t]);
        //println!("a_x:{}",v_x[t]);
        //println!("a_y:{}",v_y[t]);


    }
    return(x,y,v_x,v_y,a_x,a_y);
}

pub fn average<T>( array : & mut Vec<Vec<T>>)
where
    T: num::Zero+ num::One+ std::ops::Div<Output = T> + std::ops::AddAssign+Clone
    //T: num::* + std::ops::Div<Output = T> + std::ops::AddAssign+Clone

{ 
    let width = (*array)[0].len();
    let height = (*array).len();
    let mut  c:T =T::zero();
    let mut av:T =T::zero();
    for i in 1..width-1
    {
        for j in 1..height-1
        {
            if (*array)[j][i].is_zero()
            {
                for k in 0..9
                {
                    if !(*array)[j-1+k/3][i-1+k%3].is_zero()
                    {
                        c+= T::one();
                        av= av +((*array)[j-1+k/3][i-1+k%3]).clone();
                    }

                }
                av = av/c;
                (*array)[j][i]=av;
                av= T::zero();
                c = T::zero();
            }
        }
    }

}
/* 
pub fn average( array : & mut Vec<Vec<f64>>)
{ 
    let width = (*array)[0].len();
    let height = (*array).len();
    let mut c =0.0;
    let mut av=0.0;
    for i in 1..width-1
    {
        for j in 1..height-1
        {
            if (*array)[j][i]==0.0
            {
                for k in 0..8
                {
                    if (*array)[j-1+k/3][i-1+k%3]!=0.0
                    {
                        c+=1.0;
                        av+=(*array)[j-1+k/3][i-1+k%3];
                    }

                }
                av = av/c;
                (*array)[j][i]=av;
                av= 0.0;
                c = 0.0;
            }
        }
    }

}
// */
pub fn field_value(width: usize, height: usize, duration:usize)->Vec<Vec<Vec<f64>>>
{
    let mut electric_field:Vec<Vec<Vec<f64>>> = vec![vec![vec![0.0;width];height];duration];
    let motion = motion(width,height,duration);
    let radius = radius(width,height);
    let mut x;
    let mut x_2;
    let mut y;
    let mut pos_x;
    let mut pos_y;
    let mut r;
    let mut n_x;
    let mut n_y;
    let mut beta_x;
    let mut beta_x_2;
    let mut beta_y;
    let mut beta_dot_x;
    let mut beta_dot_y;
    let mut scalar_product;
    let mut beta_square;
    let mut gamma_2;
    let mut factor;

    let c= 10;
    let one_over_c= 1.0/(c as f64);
    
    for t in 0..duration
    {
        for i in 1..width-1
        {
            pos_x=motion.0[t];
            //x=i as i32 - (pos_x-(width/2) as i32);
            x=i as i32 - pos_x;
            x_2=x*x;
            beta_x=motion.2[t]*one_over_c;
            beta_x_2=beta_x*beta_x; 
            beta_dot_x=motion.4[t]*one_over_c;
            for j in 1..height-1
            {
                pos_y=motion.1[t];
                beta_y=motion.3[t]*one_over_c;
                beta_dot_y=motion.5[t]*one_over_c;
                beta_square = beta_x_2 + beta_y*beta_y;
                gamma_2= 1.0-beta_square;
                //y= j as i32 - (pos_y-(height/2) as i32);
                y= j as i32 - pos_y;

                r= (( ( x_2 + y*y ) as f64).sqrt()).round() as usize;
                n_x=(x as f64)/(( ( x_2 + y*y ) as f64).sqrt());
                n_y=(y as f64)/(( ( x_2 + y*y ) as f64).sqrt());
                
                /* 
                if x<0
                {
                    n_x=radius.2[y.abs() as usize][x.abs() as usize ];
                }else
                {
                    n_x=radius.2[y.abs() as usize][x.abs() as usize ];
                }
                if y<0
                {
                    n_y=radius.3[y.abs() as usize][x.abs() as usize ];
                }else
                {
                    n_y=radius.3[y.abs() as usize][x.abs() as usize ];
                }
                // */
                scalar_product= n_x*beta_x+n_y*beta_y;
                factor = (1.0-scalar_product).powf(-3.0);
                if t + r/c  <duration 
                {   
                    //complete expression 
                    //electric_field[t+r/c][j][i]=factor*( gamma_2*(n_x-beta_x)*(radius.2[ (y.abs()) as usize ][ (x.abs()) as usize])+
                    //one_over_c* (radius.1[ (y.abs()) as usize ][ (x.abs()) as usize])*( n_x*((n_y-beta_y)*beta_dot_x -(n_x-beta_x)*beta_dot_y ) ) ) ;
                    //radiative part only
                    //electric_field[t+r/c][j][i]=factor*one_over_c*(radius.1[ (y.abs()) as usize ][ (x.abs()) as usize])*( n_x*((n_y-beta_y)*beta_dot_x-(n_x-beta_x)*beta_dot_y ) );
                    electric_field[t+r/c][j][i]=factor*one_over_c*(radius.1[ (y.abs()) as usize ][ (x.abs()) as usize])*( n_x*((n_y-beta_y)*beta_dot_x-(n_x-beta_x)*beta_dot_y ) );
                    //println!("electric_field[{}][{}][{}]:{}",t+r/c,j,i,electric_field[t+r/c][j][i]);
                }
            }
        }
        average(& mut (electric_field[t]));
    }
    electric_field
}

/* 
pub fn field_value(width: usize, height: usize, duration:usize)->Vec<Vec<Vec<f64>>>
{
    let mut electric_field:Vec<Vec<Vec<f64>>> = vec![vec![vec![0.0;width];height];duration];
    let motion = motion(width,height,duration);
    let radius = radius(width,height);
    let mut x;
    let mut y;
    let mut pos_x;
    let mut pos_y;
    let mut r;
    let mut n_x;
    let mut n_y;
    let c= 10;
    let one_over_c= 1.0/(c as f64);

    for t in 0..duration
    {
        for i in 0..width
        {
            pos_x=motion.0[t];
            //x=i as i32 - (pos_x-(width/2) as i32);
            x=i as i32 - pos_x;
            for j in 0..height
            {
                pos_y=motion.1[t];
                //y= j as i32 - (pos_y-(height/2) as i32);
                y= j as i32 - pos_y;

                r= (( ( x*x + y*y ) as f64).sqrt()).round() as usize;
                if t + r/c  <duration 
                {    
                    electric_field[t+r/c][j][i]=radius.1[ (y.abs()) as usize ][ (x.abs()) as usize];

                }
            }
        }
        average(& mut (electric_field[t]));
    }
    electric_field
}
// */

pub fn field_to_buffer(electric_field: &Vec<Vec<Vec<f64>>>)->Vec<Vec<u32>>
{
    let duration = (*electric_field).len();
    let height = (*electric_field)[0].len();
    let width = (*electric_field)[0][0].len();

    let mut buffer:Vec<Vec<u32>>=vec![vec![0; width*height];duration];
    let mut max_value=0.0;
    let mut min_value=0.0;

    //search the max value and the min value;
    for t in 0..duration
    {
        for i in 0..width
        {
            for j in 0..height
            {
                if electric_field[t][j][i]>max_value
                {
                    //println!("max value:{}", max_value);
                    max_value= electric_field[t][j][i];
                }
                if electric_field[t][j][i]<min_value
                {
                    min_value= electric_field[t][j][i];
                }
            }
        }
    }

    //normalisation
    if max_value < -min_value
    {
        max_value = -min_value;
    }
    println!("max value:{}", max_value);


    for t in 0..duration
    {
        for j in 0..height
        {
            for i in 0..width
            {
                if electric_field[t][j][i]> 0.0
                {
                    buffer[t][i+j*width ] = ( ( (electric_field[t][j][i]/max_value)*65536.0*64.0 ).round() as u32)&0x00_00_FF_00;
                    //buffer[t][i+j*width ] = ( ( (electric_field[t][j][i])*65536.0/0.000000012108865848427597
                //).round() as u32)&0x00_00_FF_00;
                }else if electric_field[t][j][i]< 0.0
                {
                    buffer[t][i+j*width ] = (( (-electric_field[t][j][i]/max_value)*256.0*64.0).round() as u32)&0x00_00_00_FF;
                    //buffer[t][i+j*width ] = (( (-electric_field[t][j][i])*256.0/0.000000012108865848427597
                //).round() as u32)&0x00_00_00_FF;
                }
                
            }
        }
    }
    buffer

}


