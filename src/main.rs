
mod methods;
use crate::methods::*;

const WIDTH: usize = 640;
const HEIGHT: usize = 480;

fn main() 
{
    //let buffer_len=1_000;
    //let mut buffer: Vec<Vec<u32>> = vec![vec![0; WIDTH * HEIGHT];buffer_len];
        //prototype buffer
        /*
        for j in 0..buffer_len
        {  
            for i in 0..WIDTH*HEIGHT 
            {
                //buffer[i] = (( i as u32)%255)*( 1<<(8*((i/256)%3)) );
                buffer[j][i] = (i as u32 + 100*(j as u32) )&0x00_00_FF_FF;
                //buffer[i] = i as u32; 
            }
        }
        // */
    let duration=400;
    let electric_field = field_value(WIDTH, HEIGHT, duration);
    let buffer = field_to_buffer(&electric_field);

    display(&buffer,WIDTH,HEIGHT);
}
